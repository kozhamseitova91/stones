package Interfaces;

import com.company.Stones;

public interface Stone {

    public String getStone();
    public String getType();
    enum PreciousStones{};
    enum SemiPreciousStones{};

}
