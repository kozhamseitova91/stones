package Interfaces;

import com.company.Stones;

import java.util.List;

public interface Necklace{
    double getFullCost();

    List<Stone> getStones();
    void addStones(Stones stone);
}
