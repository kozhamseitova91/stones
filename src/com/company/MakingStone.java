package com.company;

import Interfaces.Stone;

import java.util.Arrays;
import java.util.List;

public abstract class MakingStone implements Stone {

    public Stones getStones(Stones stones){
        if (PreciousStones.getNames().contains(stones.name))
            return new Precious(stones);
        if (SemiPreciousStones.getNames().contains(stones.name))
            return new SemiPrecious(stones);
        return null;

    }

    enum PreciousStones{
        DIAMOND("Diamond"), EMERALD("Emerald"), RUBY("Ruby"), SAPPHIRE("Sapphire");
        private String name;

        PreciousStones(String name){
            this.name=name;
        }

        public static List<String> getNames(){
            return Arrays.asList(DIAMOND.getName(),EMERALD.getName(), RUBY.getName(), SAPPHIRE.getName());
        }
        public String getName() {
            return name;
        }
    }
    enum SemiPreciousStones{
        AQUAMARINE("Aquamarine"), AMETHYST("Amethyst"), AMBER("Amber"), KYANITE("Kyanite");
        private String name;

        SemiPreciousStones(String name){
            this.name=name;
        }

        public static List<String> getNames(){
            return Arrays.asList(AQUAMARINE.getName(),AMETHYST.getName(), AMBER.getName(), KYANITE.getName());
        }
        public String getName() {
            return name;
        }
    }
}
