package com.company;

import Interfaces.Necklace;
import Interfaces.Stone;

import java.util.LinkedList;
import java.util.List;

public class Jewelry extends Stones implements Necklace {
    private double fullCost;
    private List<Stone> stones = new LinkedList<>();

    @Override
    public double getFullCost() {
        fullCost += Stones.cost;
        return fullCost;
    }

    @Override
    public List<Stone> getStones() {
        return stones;
    }

    @Override
    public void addStones(Stones stone) {
        stones.add(stone);
    }

    @Override
    public String getType() {
        return null;
    }

}
