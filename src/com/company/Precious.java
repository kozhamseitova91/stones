package com.company;

public class Precious extends Stones {
    public Precious(Stones stones){
        super(stones.name, stones.weight, stones.cost);
    }

    public Precious() {

    }

    @Override
    public String getType() {
        return "Precious";
    }

}
