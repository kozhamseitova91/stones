package com.company;

import Interfaces.Stone;

public class Main {

    public static void main(String[] args) {
	 Jewelry jewelry = new Jewelry();
	 jewelry.addStones(new Precious());
	 jewelry.addStones(new SemiPrecious());

	 jewelry.getStones().forEach(c -> System.out.println(c.getType()));

		System.out.println(jewelry.getFullCost());
    }
}
