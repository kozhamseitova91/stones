package com.company;

public class SemiPrecious extends Stones {
    public SemiPrecious(Stones stones){
        super(stones.name, stones.weight, stones.cost);
    }

    public SemiPrecious() {

    }

    @Override
    public String getType() {
        return "Semi-Precious";
    }
}
