package com.company;

import Interfaces.Stone;

public abstract class Stones implements Stone {
    String name;
    double weight;
    static double cost;

    public Stones(){

    }

    public Stones (String name, double weight, double cost){
        setName(name);
        setWeight(weight);
        setCost(cost);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String getStone(){
        return name + " " + weight + " " + cost;
    }

    public abstract String getType();
}
